package com.khooroos.alltoastcomplete;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoast.StyleableToast;

public class ToastHelper {
    public void SimpleToast(Context context) {
        StyleableToast.makeText(context, "Hello World!", Toast.LENGTH_LONG, R.style.mytoast).show();
    }

    public void coloredToast(Context context, String text, int color2,int color3,int color4) {
        Toast toast = null;
        toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
        View view = toast.getView();
        view.setBackgroundColor(Color.rgb(color3,color4,color2));
        TextView toastMessage = (TextView) toast.getView().findViewById(android.R.id.message);
        toastMessage.setAllCaps(false);
        toastMessage.setTextColor(Color.rgb(color2,color3,color4));
    }


}
