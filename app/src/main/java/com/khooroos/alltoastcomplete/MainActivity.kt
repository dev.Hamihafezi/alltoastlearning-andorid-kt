package com.khooroos.alltoastcomplete

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {
    val toastHelper:ToastHelper
        get() {
            TODO()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn1.setOnClickListener {
            Toast.makeText(this, "the simple one", Toast.LENGTH_SHORT).show()
        }
        btn2.setOnClickListener {
            toast("the simple one N2 with simple code")
        }
        btn3.setOnClickListener {
            // Colored Toast
            toastHelper.coloredToast(applicationContext,"ColoredToast",111,222,156)
        }
        btn4.setOnClickListener {

        }
        btn5.setOnClickListener {
            // custom toast

        }

        btn6.setOnClickListener {

        }
        btn7.setOnClickListener {


        }
        btn8.setOnClickListener {


        }
        btn9.setOnClickListener {


        }
        btn10.setOnClickListener {


        }
    }
}